package filemgr;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;

import javax.servlet.jsp.PageContext;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import up6.biz.*;
import up6.database.DBConfig;
import up6.database.DBFile;
import up6.database.DbFolder;
import up6.store.FileBlockWriter;
import up6.PathTool;
import up6.WebBase;
import up6.ConfigReader;
import up6.CryptoTool;
import up6.model.FileInf;
import up6.sql.SqlExec;
import up6.sql.SqlParam;
import up6.sql.SqlWhereMerge;

/**
 * 文件管理器页面逻辑
 * @author zysoft
 *
 */
public class PageFileMgr {
	public PageFileMgr(){
	}

	public JSONObject rename_file(String pid,String id,String name)
	{
        DBConfig cfg = new DBConfig();
		boolean ret = cfg.db().existSameFile(name, pid);
        if (ret)
        {
        	JSONObject v = new JSONObject();
        	v.put("state", false);
        	v.put("msg", "存在同名文件");
        	v.put("code", "102");
            //var v = new JObject { { "state",false},{ "msg","存在同名文件"},{ "code","102"} };
            return v;
        }
        else
        {
            SqlExec se = new SqlExec();
            //更新相对路径：/test/test.jpg
            JSONObject info = se.read("up6_files", "f_pathRel", new SqlParam[] {new SqlParam("f_id",id)});
            String pathRel = info.getString("f_pathRel");
            Integer pos = pathRel.lastIndexOf('/');
            pathRel = pathRel.substring(0, pos+1);
            pathRel = pathRel.concat(name);
            
            //更新名称
            se.update("up6_files",
                new SqlParam[] {
                    new SqlParam("f_nameLoc",name),
                    new SqlParam("f_nameSvr",name),
                    new SqlParam("f_pathRel",pathRel)
                },
                new SqlParam[] {
                    new SqlParam("f_id",id)
                }
                );

        	JSONObject v = new JSONObject();
        	v.put("state", true);
			return v;
        }		
	}
	
	public JSONObject rename_folder(String  pid,String id,String name)
	{
        DBConfig cfg = new DBConfig();
		boolean ext = cfg.folder().existSameFolder(name, pid);
        if(ext)
        {
        	JSONObject v = new JSONObject();
        	v.put("state", false);
        	v.put("msg", "存在同名目录");
        	v.put("code", "102");
            return v;
        }
        else
        {
            //取pathRoot=/test，并构造新路径
            String pathRelNew = DbFolder.getPathRel(id);
            String pathRelOld = pathRelNew;//旧的路径,pathRoot=/test
            Integer index = pathRelNew.lastIndexOf('/');
            pathRelNew = pathRelNew.substring(0, index + 1);
            pathRelNew = pathRelNew + name;

            //更新文件名称
            SqlExec se = new SqlExec();
            if(StringUtils.isEmpty(pid) )//根目录
            {
                se.update("up6_files",
                    new SqlParam[] {
                    new SqlParam("f_nameLoc",name),
                    new SqlParam("f_nameSvr",name)
                    },
                    new SqlParam[] {
                    new SqlParam("f_id",id)
                    }
                    );

                //更新文件夹路径
                se.update("up6_files", new SqlParam[] {
                    new SqlParam("f_pathRel",pathRelNew)
                },
                new SqlParam[] {
                    new SqlParam("f_id",id)
                }
                );
            }
            else
            {
                se.update("up6_folders",
                    new SqlParam[] {
                    new SqlParam("f_nameLoc",name),
                    },
                    new SqlParam[] {
                    new SqlParam("f_id",id)
                    }
                    );

                //更新文件夹路径
                se.update("up6_folders", new SqlParam[] {
                    new SqlParam("f_pathRel",pathRelNew)
                },
                new SqlParam[] {
                    new SqlParam("f_id",id)
                }
                );
            }
            
            DBConfig dc = new DBConfig();
            dc.db().updatePathRel(pathRelOld, pathRelNew);
            dc.folder().updatePathRel(pathRelOld, pathRelNew);

            JSONObject v = new JSONObject();
            v.put("state", true);
            return v;
        }		
	}
}