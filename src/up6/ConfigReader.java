package up6;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;

import net.sf.json.JSONObject;
import up6.store.*;
import up6.store.fastdfs.*;

public class ConfigReader {
	public JSONObject m_files;
	public PathTool m_pt;
	public ReadContext m_jp;
	
	public ConfigReader()
	{
		this.m_pt = new PathTool();
		//自动加载/config.json
		String path = this.m_pt.MapPath("/config/config.json");
		String json = FileTool.readAll(path);		
		this.m_files = JSONObject.fromObject(json);
		this.m_jp = JsonPath.parse(json);
	}
	
	/**
	 * 将配置加载成一个json对象
	 * @param name
	 * @return
	 */
	public JSONObject module(String name)
	{
		String path = this.m_jp.read(name);
		XDebug.Output(path);
		path = this.m_pt.MapPath(path);
		String data = FileTool.readAll(path);
		return JSONObject.fromObject(data);
	}	
	
	/**
	 * 将JSON值转换成一个路径
	 * @param name
	 * @return
	 */
	public String readPath(String name)
	{
        Object o = this.m_jp.read(name);
        String v = o.toString();
        v = this.m_pt.MapPath(v);
        return v;
	}
	
	public JSONObject read(String name)
	{
		Object o = this.m_jp.read(name);
		return JSONObject.fromObject(o);
	}

	/**
	 * 从config.json中取一个字符串值
	 * @param name
	 * @return
	 */
	public String readString(String name)
	{
        Object o = this.m_jp.read(name);
        return o.toString();
	}

	/**
	 * 以jsonpath方式读取config.json中的bool值
	 * @param name
	 * @return
	 */
	public boolean readBool(String name)
	{
		Object o = this.m_jp.read(name);
		return Boolean.parseBoolean(o.toString());
	}

	/**
	 * 传输加密
	 * @return
	 */
	public static boolean postEncrypt() {
		ConfigReader cr = new ConfigReader();
		return cr.readBool("security.encrypt");
	}

    public static boolean securityToken() {
		ConfigReader cr = new ConfigReader();
		return cr.readBool("security.token");
    }
	/**
	 * 获取存储类型
	 * @return
	 */
	public static StorageType storage(){
		ConfigReader cr = new ConfigReader();
		String sto = cr.readString("Storage.type");
		if(sto.compareToIgnoreCase("FastDFS") ==0) return StorageType.FastDFS;
		if(sto.compareToIgnoreCase("Minio") ==0) return StorageType.Minio;
		return StorageType.IO;		
	}

	/**
	 * 存储加密
	 * @return
	 */
	public static boolean storageEncrypt(){
		ConfigReader cr = new ConfigReader();
		return cr.readBool("Storage.encrypt");
	}
	
	/**
	 * 块数据写入器
	 * @return
	 */
	public static FileBlockWriter blockWriter(){
		FileBlockWriter w = null;
		StorageType st = ConfigReader.storage();
		if(st==StorageType.FastDFS) w = new FastDFSWriter();
		else  w = new FileBlockWriter();
		return w;
	}
	
	/**
	 * 块数据读取器
	 * @return
	 */
	public static FileBlockReader blockReader(){
		FileBlockReader w = null;
		StorageType st = ConfigReader.storage();
		if(st==StorageType.FastDFS) w = new FastDFSReader();
		else  w = new FileBlockReader();
		return w;
	}
}