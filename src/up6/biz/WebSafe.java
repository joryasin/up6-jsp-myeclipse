package up6.biz;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONObject;
import up6.ConfigReader;
import up6.CryptoTool;
import up6.model.FileInf;


public class WebSafe {
	
	public WebSafe(){}
	
	/// <summary>
    /// 验证token
    /// </summary>
    /// <param name="token"></param>
    /// <param name="f"></param>
    /// <returns></returns>
	public boolean validToken(String token, FileInf f)
	{
		String action = "init";
		//加密
		if (ConfigReader.securityToken())
		{
			if(StringUtils.isBlank(token.trim())) return false;
			CryptoTool ct = new CryptoTool();
			return StringUtils.equals(ct.token(f,action), token);
		}
		return true;
	}

	/// <summary>
    /// 验证token
    /// </summary>
    /// <param name="token"></param>
    /// <param name="f"></param>
    /// <returns></returns>
	public boolean validToken(String token, FileInf f,String action)
	{
		//加密
		if (ConfigReader.securityToken())
		{
			if(StringUtils.isBlank(token.trim())) return false;
			CryptoTool ct = new CryptoTool();
			return StringUtils.equals(ct.token(f,action), token);
		}
		return true;
	}
}
