package up6.biz;

import com.google.gson.Gson;
import up6.FileTool;
import up6.PathTool;
import up6.model.FileInf;

public class FileEtag {

    /**
     *
     * @param f
     */
    public void saveTags(FileInf f) {
        String file = f.ETagsFile();
        PathTool.mkdirsFromFile(file);
        //System.out.println("ETag.file="+file);

        Gson g = new Gson();
        String val = g.toJson(f);
        FileTool.appendLine(file, val);
    }
}
