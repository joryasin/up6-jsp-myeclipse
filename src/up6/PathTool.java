package up6;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

public class PathTool {
	
	/**
	 * 在linux中，getName()取出来的文件名称包含路径。需要手动去掉路径部分。
	 * @param n
	 * @return
	 */
	public static String getName(String n){
		File f = new File(n);
		int pos = f.getName().lastIndexOf("\\");
		if(pos == -1) pos = f.getName().lastIndexOf("/");
		return f.getName().substring(pos+1);
	}
	
	public static String[] split(String str,String p)
	{
		List<String> arr = new ArrayList<String>();
		int posPre=0;
		int pos = str.indexOf(p);
		while(-1 != pos)
		{
			arr.add(str.substring(posPre,pos));
			posPre = pos+1;
			pos = str.indexOf(p, pos+1);
		}
		
		return arr.toArray(new String[0]);		
	}
	
	public static String guid(){
		return UUID.randomUUID().toString().replace("-","");
	}
	
	public static String getParent(String ps)
	{
		File f = new File(ps);
		return f.getParent();
	}

    public static String combin(String a,String b)
    {
        a = a.replace('\\', '/');
        b = b.replace('\\', '/');
        if (a.endsWith("/")) a = a.substring(0, a.length() - 1);
        if (b.startsWith("/")) b = b.substring(1);
		if (b.startsWith("\\")) b = b.substring(1);
        return a + "/" + b;
    }
	
	public static String getExtention(String n){		
		if(n.indexOf(".")==-1) return "";
		
		String name = getName(n);

		int extIndex = name.lastIndexOf(".");
		//有扩展名
		if(-1 != extIndex)
		{
			String ext = name.substring(extIndex + 1);
			return ext;
		}
		return "";
	}
	
	public static Boolean exist(String v)
	{
		File f = new File(v);
		return f.exists();
	}
	
	public static boolean createDirectory(String v){

		File fd = new File(v);		
		//fix():不创建文件夹
		if(!fd.exists()) return fd.mkdirs();
		return true;
	}

	public static void mkdirsFromFile(String f)
	{
		String dir = getParent(f);
		PathTool.createDirectory(dir);
	}
	
	//规范化路径，与操作系统保持一致。
	public static String canonicalPath(String v) throws IOException{
		File f = new File(v);
		return f.getCanonicalPath();
	}

	public static String url_decode(String v)
	{
		v = v.replace("+","%20");	
		try {
			v = URLDecoder.decode(v,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//utf-8解码//客户端使用的是encodeURIComponent编码，
		return v;
	}

	public static String url_safe_encode(String v)
	{
		try {
			v = URLEncoder.encode(v,"UTF-8");
			v = v.replace("+", "%20");
			v = v.replace("*", "%2A");
			v = v.replace("%7E", "~");
			v = v.replace("%2F", "/");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//utf-8解码//客户端使用的是encodeURIComponent编码，
		return v;
	}
	
	/**
	 * 将相对路径转换成绝对路径
	 * @param ps res
	 * @return /up6/imgs
	 */
	public String MapPath(String ps)
	{		
		// /up6
		String root = this.getRoot();
		
		if(StringUtils.isBlank(ps)) return root;
		
		//传值： /imgs
		if(!StringUtils.equals("/", ps.substring(0, 1))) root.contains("/");
		
		root = root.concat(ps);
		
		File f = new File(root);
		String path="";
		try {
			path = f.getCanonicalPath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return path;
	}
	
	/**
	 * 获取项目根目录
	 * @return D:/app/tomcat/apache-tomcat-8.5.61/webapps/up6/
	 */
	public String getRoot() 
	{
		String path = this.getClass().getResource("/").getPath();
		path = path.replaceAll("//", "/");
		if(path.startsWith("/")) path = path.substring(1);
		//D:/app/tomcat/apache-tomcat-8.5.61/webapps/up6/WEB-INF/classes/
		int pos = path.indexOf("WEB-INF/classes/");
		return path.substring(0,pos);
	}

	
	public static String BytesToString(long byteCount)
	{
	    String[] suf = { "B", "KB", "MB", "GB", "TB", "PB", "EB" };
	    if (byteCount == 0)
	        return "0" + suf[0];
	    long bytes = Math.abs(byteCount);
	    int place = (new Double(Math.floor(Math.log(bytes) / Math.log(1024))).intValue());
	    double num = (double)bytes / Math.pow(1024, place);
	    num = new BigDecimal(num).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
	    return String.valueOf( (Math.signum(byteCount) * num) ) + suf[place];
	}
}