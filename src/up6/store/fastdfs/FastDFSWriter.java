package up6.store.fastdfs;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.lang.StringUtils;

import up6.PathTool;
import up6.model.FileInf;
import up6.store.*;

public class FastDFSWriter extends FileBlockWriter{

    public FastDFSWriter() {
        this.storage=StorageType.FastDFS;
    }

    public String make(FileInf file) throws IOException {
        String fileID="";
        String ext = PathTool.getExtention(file.pathSvr);
        byte[] buf = new byte[0];
        fileID = FastDFSTool.upload(buf, ext);
        if(StringUtils.isEmpty(fileID)) throw new IOException("FastDFS make file error");
        return fileID;
    }

    /**
     * 参数：pathSvr
     */
    public String write(FileInf file, ByteArrayOutputStream ostm) throws IOException {
        byte[] data = ostm.toByteArray();
        if(FastDFSTool.write(file.object_id, data)) return "ok";
        else throw  new IOException("FastDFS upload part error");
    }
    
    /**
     * 写入最后一块数据=>所有文件块上传完毕
     * @param file
     * @return
     */
    public boolean writeLastPart(FileInf file){
        return  true;
    }
}