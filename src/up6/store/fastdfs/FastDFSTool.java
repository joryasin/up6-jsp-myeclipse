package up6.store.fastdfs;

import java.io.*;

import org.apache.commons.fileupload.FileItem;
import org.csource.common.MyException;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.FileInfo;
import org.csource.fastdfs.StorageClient1;
import org.csource.fastdfs.StorageServer;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;
import up6.ConfigReader;
import up6.FileTool;

public class FastDFSTool {
	private static boolean inited=false;

    public FastDFSTool() { }

    public static void init(){
		try {
			if(!FastDFSTool.inited) {
				ConfigReader cr = new ConfigReader();
				ClientGlobal.init(cr.readPath("FastDFS"));
				FastDFSTool.inited = true;
				System.out.println("FastDFSTool.init.初始化FastDFS配置");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MyException e) {
			e.printStackTrace();
		}
	}

	public static StorageClient1 client() {
    	/*
		DfsConnectPool pool = DfsConnectPool.getPool();
		StorageClient1 sc = pool.checkout(10);
		return sc;
    	 */
		FastDFSTool.init();
		StorageClient1 c=null;
			try {
		        TrackerClient tracker = new TrackerClient();
		        TrackerServer trackerSvr = tracker.getTrackerServer();
		        StorageServer storageSvr = null;
		        c = new StorageClient1(trackerSvr, storageSvr);
					
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return c;
	}

	public static void save(StorageClient1 c){
		//DfsConnectPool pool = DfsConnectPool.getPool();
		//pool.checkin(c);
	}
	
	public static FileInfo query(String id)
	{
		FileInfo f=null;
		StorageClient1 c = client();
		try {
			f = c.query_file_info1(id);
			save(c);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return f;
	}
	
	public static String upload_test()
	{
		 try {
	            StorageClient1 cli = client();
//	          NameValuePair nvp = new NameValuePair("age", "18"); 
	            NameValuePair nvp [] = new NameValuePair[]{ 
	                    new NameValuePair("age", "18"), 
	                    new NameValuePair("sex", "male") 
	            };
	            byte[] buf = FileTool.readAllBytes("f:\\test.txt");
	            String idSvr = cli.upload_file1(buf, "txt", nvp);
	            
	            //System.out.println(fileIds.length); 
	            //System.out.println("组名：" + fileIds[0]); 
	            System.out.println(idSvr);	            
	            return idSvr;

	        } catch (FileNotFoundException e) { 
	            e.printStackTrace(); 
	        } catch (IOException e) { 
	            e.printStackTrace(); 
	        } catch (MyException e) { 
	            e.printStackTrace(); 
	        }
		return null; 
	}
	
	public static String upload(byte[] buf,String ext)
	{		
		String file_id="";
		StorageClient1 client = client();
		NameValuePair nvp [] = new NameValuePair[]{};		
		try {
			file_id = client.upload_appender_file1(buf, ext, nvp);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file_id;
	}
	
	public static int write(String fileID,long offset,FileItem block)
	{		
		int blockSize = 0;
		try 
		{			
			blockSize = (int)block.getSize();
			InputStream stream = block.getInputStream();			
			byte[] data = new byte[blockSize];
			if(stream.read(data) != blockSize)
			{
				throw new IOException();
			}
			stream.close();
			
			StorageClient1 cli = client();
			cli.append_file1(fileID,data);			
			
		} catch (IOException e) {
			blockSize = 0;
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MyException e) {
			blockSize = 0;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return blockSize;
	}
	
	public static boolean write(String fileID,byte[] data)
	{
		try 
		{
			StorageClient1 c = client();
			c.append_file1(fileID,data);
			save(c);
			return true;
		} catch (IOException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MyException e) {
			
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return false;
	}
	
	public static byte[] down(String fileID,long offset,long size)
	{
		byte[] buf = new byte[0];
		StorageClient1 c = client();
		try {
			buf = c.download_file1(fileID,offset,size);
			return buf;
		} catch (IOException e) {
			if(c!=null) save(c);
			// TODO Auto-generated catch block
			System.out.println("FastDFSTool.down.error");
			e.printStackTrace();
		} catch (MyException e) {
			// TODO Auto-generated catch block
			System.out.println("FastDFSTool.down.error");
			e.printStackTrace();
		}
		return buf;
	}
}