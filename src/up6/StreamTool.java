package up6;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.fileupload.FileItem;

public class StreamTool {
	public static byte[] toBytes(FileItem f) {
		byte[] buf = null;
		try {
			InputStream stream = f.getInputStream();			
			buf = new byte[(int)f.getSize()];
			stream.read(buf);
			stream.close();		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return buf;
	}	
	
	public static ByteArrayOutputStream toStream(FileItem b)
	{
		ByteArrayOutputStream s =null;
		try {
			s = new ByteArrayOutputStream();
			s.write(toBytes(b));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}
}
