<%@ page language="java" import="up6.*" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="com.google.gson.Gson" %>
<%@ page import="up6.model.*" %>
<%@ page import="up6.biz.*" %>
<%@	page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="net.sf.json.JSONObject" %>
<%@ page import="up6.store.FileBlockWriter" %>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.store.StorageType" %>
<%@ page import="up6.sql.SqlWhereMerge" %>
<%@ page import="up6.sql.SqlExec" %>
<%@ page import="net.sf.json.JSONArray" %>
<%@ page import="filemgr.PageFileMgr" %>
<%@ page import="up6.database.DbFolder" %>
<%@ page import="java.io.UnsupportedEncodingException" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);


    String id		= web.queryString("id");
    String callback = web.queryString("callback");

    if (StringUtils.isBlank(id))
    {
        web.toContent(callback + "({\"value\":null})");
        return;
    }
    FolderBuilder fb = new FolderBuilder();
    Gson gson = new Gson();
    String json = gson.toJson(fb.build(id));

    try {
        json = URLEncoder.encode(json,"UTF-8");
    } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }//编码，防止中文乱码
    json = json.replace("+","%20");
    json = callback + "({\"value\":\"" + json + "\"})";//返回jsonp格式数据。
    out.write(json);
%>