<%@ page language="java" import="org.apache.commons.lang.StringUtils" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="up6.WebBase" %>
<%@ page import="up6.biz.FolderSchemaDB" %>
<%@ page import="up6.biz.up6_biz_event" %>
<%@	page import="up6.database.DBConfig" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.database.DbFolder" %>
<%@ page import="up6.model.FileInf" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);

    String id		= web.reqToString("id");
    String uid 		= web.reqToString("uid");
    String cbk 		= web.reqToString("callback");//jsonp
    int cover		= web.reqToInt("cover");//是否覆盖
    int ret = 0;

    //参数为空
    if (	!StringUtils.isBlank(uid)||
            !StringUtils.isBlank(id))
    {
        //取当前节点信息
        DBConfig cfg = new DBConfig();
        DbFolder db = cfg.folder();
        FileInf folder = db.read(id);
        folder.uid = Integer.parseInt(uid);
        FileInf fdExist = db.read(folder.pathRel, folder.pid, id);
        if(1==cover && fdExist !=null)
        {
            folder.id = fdExist.id;
            folder.pid = fdExist.pid;
            folder.pidRoot = fdExist.pidRoot;
        }

        //根节点
        FileInf root = new FileInf();
        root.id = folder.pidRoot;
        root.uid = folder.uid;
        //当前节点是根节点
        if( StringUtils.isBlank(root.id)) root.id = folder.id;


        //上传完毕
        DBFile db2 = cfg.db();
        db2.fd_complete(id,uid);

        FolderSchemaDB fsd = new FolderSchemaDB();
        fsd.save(folder);

        //清理同名子文件
        if(1==cover && fdExist !=null)
        {
            //覆盖同名子文件
            fsd.cover();
        }

        //添加文件记录
        //sa.scan(folder,folder.pathSvr);

        cfg.db().fd_scan(id,uid);

        up6_biz_event.folder_post_complete(id);

        //删除当前目录
        if(1 == cover && fdExist != null)
        {
            DbFolder.del(id, Integer.parseInt(uid));
        }

        ret = 1;
    }
    out.write(cbk + "(" + ret + ")");
%>