<%@ page language="java" import="up6.*" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="com.google.gson.Gson" %>
<%@ page import="up6.model.*" %>
<%@ page import="up6.biz.*" %>
<%@	page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="net.sf.json.JSONObject" %>
<%@ page import="up6.store.FileBlockWriter" %>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.store.StorageType" %>
<%@ page import="up6.sql.SqlWhereMerge" %>
<%@ page import="up6.sql.SqlExec" %>
<%@ page import="net.sf.json.JSONArray" %>
<%@ page import="filemgr.PageFileMgr" %>
<%@ page import="up6.database.DbFolder" %>
<%@ page import="up6.sql.SqlParam" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);

    String name = web.reqToString("f_nameLoc");
    name = PathTool.url_decode(name);
    String pid = web.reqToString("f_pid");
    int uid = web.reqToInt("uid");
    String pidRoot = web.reqToString("f_pidRoot");
    String pathRel = web.reqToString("f_pathRel");
    pathRel = PathTool.url_decode(pathRel);
    pathRel = PathTool.combin(pathRel, name);

    DBConfig cfg = new DBConfig();
    DbFolder df = cfg.folder();
    if (df.exist_same_folder(name, pid))
    {
        JSONObject ret = new JSONObject();
        ret.put("ret", false);
        ret.put("msg", "已存在同名目录");
        out.write(ret.toString());
        return;
    }

    SqlExec se = new SqlExec();
    String guid = PathTool.guid();

    //根目录
    if ( StringUtils.isBlank(pid) )
    {
        se.insert("up6_files", new SqlParam[] {
                new SqlParam("f_id",guid)
                ,new SqlParam("f_pid",pid )
                ,new SqlParam("f_uid",uid )
                ,new SqlParam("f_pidRoot",pidRoot )
                ,new SqlParam("f_nameLoc",name )
                ,new SqlParam("f_complete",true)
                ,new SqlParam("f_fdTask",true)
                ,new SqlParam("f_pathRel",pathRel)
        });
    }//子目录
    else
    {
        se.insert("up6_folders"
                , new SqlParam[] {
                        new SqlParam("f_id",guid)
                        ,new SqlParam("f_pid",pid)
                        ,new SqlParam("f_uid",uid)
                        ,new SqlParam("f_pidRoot",pidRoot )
                        ,new SqlParam("f_nameLoc",name )
                        ,new SqlParam("f_complete",true)
                        ,new SqlParam("f_pathRel",pathRel)
                });
    }

    JSONObject ret = new JSONObject();
    ret.put("ret", true);
    ret.put("f_id", guid);
    ret.put("f_pid", pid);
    ret.put("f_uid", uid);
    ret.put("f_pidRoot", pidRoot);
    ret.put("f_nameLoc", name);
    ret.put("f_pathRel", pathRel);
    out.write(ret.toString());
%>