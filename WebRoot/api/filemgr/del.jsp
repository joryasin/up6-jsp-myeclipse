<%@ page language="java" import="up6.*" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="com.google.gson.Gson" %>
<%@ page import="up6.model.*" %>
<%@ page import="up6.biz.*" %>
<%@	page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="net.sf.json.JSONObject" %>
<%@ page import="up6.store.FileBlockWriter" %>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.store.StorageType" %>
<%@ page import="up6.sql.SqlWhereMerge" %>
<%@ page import="up6.sql.SqlExec" %>
<%@ page import="net.sf.json.JSONArray" %>
<%@ page import="up6.sql.SqlParam" %>
<%
    out.clear();
    WebBase web = new WebBase(pageContext);
    String id = web.queryString("id");
    String pathRel = web.reqStringDecode("pathRel");
    pathRel += '/';

    SqlWhereMerge swm = new SqlWhereMerge();
    DBConfig cfg = new DBConfig();
    if(StringUtils.equals(cfg.m_db, "sql"))
    {
        swm.charindex(pathRel,"f_pathRel");
    }
    else
    {
        swm.instr(pathRel,"f_pathRel");
    }
    String where = swm.to_sql();

    SqlExec se = new SqlExec();
    se.update("up6_files", new SqlParam[] {new SqlParam("f_deleted",true)}, where);
    se.update("up6_files"
            , new SqlParam[] {new SqlParam("f_deleted",true)}
            , new SqlParam[] {new SqlParam("f_id",id)}
    );
    se.update("up6_folders", new SqlParam[] {new SqlParam("f_deleted",true)}, where);
    se.update("up6_folders"
            , new SqlParam[] {new SqlParam("f_deleted",true)}
            , new SqlParam[] {new SqlParam("f_id",id)}
    );

    JSONObject ret = new JSONObject();
    ret.put("ret", 1);
    out.write(ret.toString());
%>