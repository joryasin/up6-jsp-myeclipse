<%@ page language="java" import="up6.*" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="com.google.gson.Gson" %>
<%@ page import="up6.model.*" %>
<%@ page import="up6.biz.*" %>
<%@	page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="net.sf.json.JSONObject" %>
<%@ page import="up6.store.FileBlockWriter" %>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.store.StorageType" %>
<%@ page import="up6.sql.SqlWhereMerge" %>
<%@ page import="up6.sql.SqlExec" %>
<%@ page import="net.sf.json.JSONArray" %>
<%@ page import="filemgr.PageFileMgr" %>
<%@ page import="up6.database.DbFolder" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.UnsupportedEncodingException" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);

    String id       = web.queryString("id");
    String pid      = web.queryString("pid");
    String pidRoot  = web.queryString("pidRoot");
    if( StringUtils.isBlank(pidRoot)) pidRoot = pid;//父目录是根目录
    String uid      = web.queryString("uid");
    String lenLoc   = web.queryString("lenLoc");
    String sizeLoc  = web.queryString("sizeLoc");
    String pathLoc  = web.queryString("pathLoc");
    String pathRel  = web.queryString("pathRel");
    pathRel 		= PathTool.url_decode(pathRel);
    pathLoc			= PathTool.url_decode(pathLoc);
    String callback = web.queryString("callback");//jsonp


    //参数为空
    if (StringUtils.isBlank(id)
            || StringUtils.isBlank(uid)
            || StringUtils.isBlank(pathLoc))
    {
        web.toContent(callback + "({\"value\":null})");
        return;
    }

    FileInf fileSvr = new FileInf();
    fileSvr.id      = id;
    fileSvr.pid     = pid;
    fileSvr.pidRoot = pidRoot;
    fileSvr.fdChild = false;
    fileSvr.fdTask  = true;
    fileSvr.uid     = Integer.parseInt(uid);
    fileSvr.nameLoc = PathTool.getName(pathLoc);
    fileSvr.pathLoc = pathLoc;
    fileSvr.pathRel = PathTool.combin(pathRel, fileSvr.nameLoc);
    fileSvr.lenLoc  = Long.parseLong(lenLoc);
    fileSvr.sizeLoc = sizeLoc;
    fileSvr.deleted = false;
    fileSvr.nameSvr = fileSvr.nameLoc;

    //检查同名目录
		/*DbFolder df = new DbFolder();
		if (df.exist_same_folder(fileSvr.nameLoc, pid))
		{
			JSONObject o = new JSONObject();
			o.put("value","");
			o.put("ret", false);
			o.put("code", "102");
		    String js = callback + String.format("(%s)", o.toString());
		    web.toContent(js);
		    return;
		}*/

    //生成存储路径
    PathBuilderUuid pb = new PathBuilderUuid();
    try {
        fileSvr.pathSvr = pb.genFolder(fileSvr);
    } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    fileSvr.pathSvr = fileSvr.pathSvr.replace("\\","/");
    PathTool.createDirectory(fileSvr.pathSvr);

    //保存层级结构
    FolderSchema fs = new FolderSchema();
    fs.create(fileSvr);

    //添加到数据表
    DBConfig cfg = new DBConfig();
    DBFile db = cfg.db();
    if(StringUtils.isBlank(pid)) db.Add(fileSvr);
    else db.addFolderChild(fileSvr);

    //传输加密
    if (ConfigReader.postEncrypt())
    {
        CryptoTool ct   = new CryptoTool();
        try{
            fileSvr.pathSvr = ct.encrypt(fileSvr.pathSvr);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    up6_biz_event.folder_create(fileSvr);

    Gson g = new Gson();
    String json = g.toJson(fileSvr);
    try {
        json = URLEncoder.encode(json,"utf-8");
    } catch (UnsupportedEncodingException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    json = json.replace("+","%20");

    JSONObject ret = new JSONObject();
    ret.put("value",json);
    ret.put("ret",true);
    json = callback + String.format("(%s)",ret.toString());//返回jsonp格式数据。
    out.write(json);
%>