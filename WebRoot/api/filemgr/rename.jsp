<%@ page language="java" import="up6.*" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="com.google.gson.Gson" %>
<%@ page import="up6.model.*" %>
<%@ page import="up6.biz.*" %>
<%@	page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="net.sf.json.JSONObject" %>
<%@ page import="up6.store.FileBlockWriter" %>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.store.StorageType" %>
<%@ page import="up6.sql.SqlWhereMerge" %>
<%@ page import="up6.sql.SqlExec" %>
<%@ page import="net.sf.json.JSONArray" %>
<%@ page import="filemgr.PageFileMgr" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);
    PageFileMgr mgr = new PageFileMgr();

    String pid     = web.reqToString("f_pid");
    String id      = web.reqToString("f_id");
    boolean folder = web.reqToBool("f_fdTask");
    String nameNew = web.reqToString("f_nameLoc");
    nameNew 	   = PathTool.url_decode(nameNew);

    JSONObject v = null;
    if (folder)
    {
        v = mgr.rename_folder(pid,id,nameNew);
    }
    else
    {
        v = mgr.rename_file(pid,id, nameNew);
    }

    out.write(v.toString());
%>