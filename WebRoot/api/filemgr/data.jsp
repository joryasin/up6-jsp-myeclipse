<%@ page language="java" import="up6.*" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="com.google.gson.Gson" %>
<%@ page import="up6.model.*" %>
<%@ page import="up6.biz.*" %>
<%@	page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="net.sf.json.JSONObject" %>
<%@ page import="up6.store.FileBlockWriter" %>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.store.StorageType" %>
<%@ page import="up6.sql.SqlWhereMerge" %>
<%@ page import="up6.sql.SqlExec" %>
<%@ page import="net.sf.json.JSONArray" %>
<%
    out.clear();
    WebBase web = new WebBase(pageContext);
    String pid = web.queryString("pid");
    String pageSize = web.queryString("limit");
    String pageIndex = web.queryString("page");
    if(StringUtils.isBlank(pageSize)) pageSize = "20";
    if(StringUtils.isBlank(pageIndex)) pageIndex = "1";
    String pathRel = web.reqStringDecode("pathRel");
    pathRel += '/';

    SqlWhereMerge swm = new SqlWhereMerge();
    DBConfig cfg = new DBConfig();
    //if( !StringUtils.isBlank(pid)) swm.equal("f_pid", pid);
    if( !StringUtils.isBlank(pid))
    {
        if(StringUtils.equals(cfg.m_db, "sql"))
        {
            swm.add("f_pathRel", String.format("f_pathRel='%s'+f_nameLoc",pathRel));
        }
        else
        {
            swm.add("f_pathRel", String.format("f_pathRel=CONCAT('%s',f_nameLoc)",pathRel));
        }
    }

    swm.equal("f_uid", web.reqToInt("uid"));
    swm.equal("f_complete", 1);
    swm.equal("f_deleted", 0);
    swm.equal("f_fdChild", 1);

    Boolean isRoot = StringUtils.isBlank(pid);
    if(isRoot) swm.equal("f_fdChild", 0);

    String where = swm.to_sql();

    //加载文件列表
    SqlExec se = new SqlExec();
    JSONArray files = se.page("up6_files"
            ,"f_id"
            , "f_id,f_pid,f_nameLoc,f_sizeLoc,f_lenLoc,f_time,f_pidRoot,f_fdTask,f_pathSvr,f_pathRel"
            , Integer.parseInt( pageSize )
            , Integer.parseInt( pageIndex )
            , where
            , "f_fdTask desc,f_time desc");

    //根目录不加载up6_folders表
    JSONArray folders = new JSONArray();
    if(!isRoot)
    {
        swm.del("f_fdChild");

        where = swm.to_sql();
        folders = se.page("up6_folders"
                , "f_id"
                , "f_id,f_nameLoc,f_pid,f_sizeLoc,f_time,f_pidRoot,f_pathRel"
                ,Integer.parseInt(pageSize)
                ,Integer.parseInt(pageIndex)
                , where
                , "f_time desc");

        for(int i = 0 , l = folders.size();i<l;++i)
        {
            JSONObject o = folders.getJSONObject(i);
            o.put("f_fdTask", true);
            o.put("f_fdChild", false);
            o.put("f_pathSvr", "");
        }
    }

    //添加文件
    for(int i = 0 , l = files.size();i<l;++i) folders.add(files.getJSONObject(i));

    int count = se.count("up6_files", where);
    if(!isRoot)
    {
        count += se.count("up6_folders", where);
    }

    JSONObject o = new JSONObject();
    o.put("count", count);
    o.put("code", 0);
    o.put("msg", "");
    o.put("data", folders);

    out.write(o.toString());
%>