<%@ page language="java" import="up6.*" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@	page import="com.google.gson.Gson" %>
<%@ page import="up6.model.*" %>
<%@ page import="up6.biz.*" %>
<%@	page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="net.sf.json.JSONObject" %>
<%@ page import="up6.store.FileBlockWriter" %>
<%@ page import="up6.database.DBConfig" %>
<%@ page import="up6.database.DBFile" %>
<%@ page import="up6.store.StorageType" %>
<%@ page import="up6.sql.SqlWhereMerge" %>
<%@ page import="up6.sql.SqlExec" %>
<%@ page import="net.sf.json.JSONArray" %>
<%@ page import="filemgr.PageFileMgr" %>
<%@ page import="up6.database.DbFolder" %>
<%
    out.clear();

    WebBase web = new WebBase(pageContext);

    String md5 		= web.reqToString("md5");
    int uid 		= web.reqToInt("uid");
    String id		= web.reqToString("id");
    String pid 		= web.reqToString("pid");
    String callback = web.reqToString("callback");//jsonp
    int cover		= web.reqToInt("cover");//是否覆盖
    String nameLoc	= web.reqToString("nameLoc");//文件名称
    nameLoc			= PathTool.url_decode(nameLoc);

    //返回值。1表示成功
    int ret = 0;
    if ( !StringUtils.isBlank(id))
    {
        DBConfig cfg = new DBConfig();
        cfg.db().complete(id);

        //覆盖同名文件-更新同名文件状态
        if(cover == 1) cfg.db().delete(pid, nameLoc, uid, id);

        up6_biz_event.file_post_complete(id);
        ret = 1;
    }
    out.write(callback + "(" + ret + ")");
%>