<%@ page language="java" import="up6.database.DBFile" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page import="up6.store.FileBlockWriter" %>
<%@ page import="up6.XDebug" %>
<%@ page import="up6.*" %>
<%@ page import="up6.model.*" %>
<%@ page import="up6.biz.*" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="org.apache.commons.fileupload.FileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.FileUploadException" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="org.apache.commons.lang.*" %>
<%@ page import="java.net.URLDecoder"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="net.sf.json.JSONObject"%>
<%@ page import="java.util.List"%>
<%@ page import="java.io.InputStream"%>
<%@ page import="java.io.ByteArrayOutputStream"%>
<%@	page import="java.io.IOException" %>
<%@ page import="up6.store.StorageType" %>
<%@ page import="sun.security.krb5.Config" %>
<%
out.clear();
/*
	此页面负责将文件块数据写入文件中。
	此页面一般由控件负责调用
	参数：
		uid
		idSvr
		md5
		lenSvr
		pathSvr
		RangePos
		fd_idSvr
		fd_lenSvr
	更新记录：
		2022-11-15 优化块接收逻辑
		2017-10-23 增加删除文件块缓存操作
		2017-07-13 取消数据库操作
		2016-04-09 优化文件存储逻辑，增加更新文件夹进度逻辑
		2015-03-19 客户端提供pathSvr，此页面减少一次访问数据库的操作。
		2014-07-23 优化代码。
		2012-10-25 整合更新文件进度信息功能。减少客户端的AJAX调用。
		2012-04-18 取消更新文件上传进度信息逻辑。
		2012-04-12 更新文件大小变量类型，增加对2G以上文件的支持。
*/
WebBase wb = new WebBase(pageContext);
String uid 			= request.getHeader("uid");//
String id 			= request.getHeader("id");
String pid 			= request.getHeader("pid");
String pidRoot		= request.getHeader("pidRoot");
String lenSvr		= request.getHeader("lenSvr");
String lenLoc		= request.getHeader("lenLoc");
String blockOffset	= request.getHeader("blockOffset");
int blockSize		= request.getIntHeader("blockSize");//原始块大小
int blockSizeCry	= request.getIntHeader("blockSizeCry");//加密块大小
boolean blockEncrypt = StringUtils.equalsIgnoreCase(request.getHeader("blockEncrypt"),"true");//是否加密
String blockIndex	= request.getHeader("blockIndex");
String blockCount   = request.getHeader("blockCount");//块总数
String blockMd5		= request.getHeader("blockMd5");
String complete		= request.getHeader("complete");
String object_id   	= request.getHeader("object_id");//
String pathSvr		= "";
String pathLoc		= "";
String pathRel		= "";
String token		= request.getHeader("token");
boolean isLastBlock = StringUtils.equals(complete, "true");

//参数为空 
if(	 StringUtils.isEmpty( uid )||
		StringUtils.isBlank( id )||
		StringUtils.isEmpty( blockOffset ))
{
	JSONObject o = new JSONObject();
	o.put("blockOffset",blockOffset);
	o.put("id",id);
	wb.send_erro("paramEmpty",o);
	return;
}

HttpRequest form = new HttpRequest(pageContext);
FileItem blockData = form.getFile("file");//文件块
FileItem thumbData = form.getFile("thumb");//缩略图，与最后一个文件块一起上传。
pathSvr = PathTool.url_decode( form.getString("pathSvr"));
pathRel = PathTool.url_decode( form.getString("pathRel"));
pathLoc = PathTool.url_decode( form.getString("pathLoc"));

boolean verify = false;
ByteArrayOutputStream ostm = StreamTool.toStream(blockData);
long blockSizeSvr = blockData.getSize();
if(StringUtils.isBlank( pathLoc )) pathLoc = blockData.getName();

FileInf fileSvr = new FileInf();
fileSvr.id = id;
fileSvr.pid = pid;
fileSvr.pidRoot = pidRoot;
fileSvr.object_id = object_id;
fileSvr.lenSvr = Long.parseLong(lenSvr);
fileSvr.lenLoc = Long.parseLong(lenLoc);
fileSvr.sizeLoc = PathTool.BytesToString(fileSvr.lenLoc);
fileSvr.pathLoc = pathLoc;
fileSvr.pathSvr = pathSvr;
fileSvr.pathRel = pathRel.replace('\\','/');
fileSvr.blockIndex = Integer.parseInt(blockIndex);
fileSvr.blockOffset = Long.parseLong(blockOffset);
fileSvr.blockSize = blockSize;//原始块大小
fileSvr.blockCount = Integer.parseInt(blockCount);
fileSvr.nameLoc = PathTool.getName(pathLoc);
fileSvr.nameSvr = fileSvr.nameLoc;
fileSvr.encrypt = ConfigReader.storageEncrypt();//加密存储

//传输加密
if (ConfigReader.postEncrypt())
{
	//接收块大小不相同
	if(blockSizeCry!= ostm.size())
	{
		JSONObject o = new JSONObject();
		o.put("blockSizeSend",blockSizeCry);
		o.put("blockSizeRecv",ostm.size());
		wb.send_erro("blockSizeCryDifferent",o);
		return;
	}
	
	CryptoTool ct   = new CryptoTool();
    fileSvr.pathSvr = ct.decrypt(pathSvr);
    //非加密存储=>还原加密数据
    if(!fileSvr.encrypt) ostm = ct.decrypt(blockData,blockSize);
    blockSizeSvr = ostm.size();
}//接收块大小不相同
else if(blockSize!= ostm.size())
{
	JSONObject o = new JSONObject();
	o.put("blockSizeSend",blockSize);
	o.put("blockSizeRecv",ostm.size());
	wb.send_erro("blockSizeDifferent",o);
	return;
}

//token验证
WebSafe ws = new WebSafe();
if(!ws.validToken(token,fileSvr,"block"))
{
	JSONObject o = new JSONObject();
	o.put("fileSvr",JSONObject.fromObject(fileSvr));
	o.put("token",token);
	wb.send_erro("tokenValidFail",o);
	return;
}

//验证文件块MD5
String md5Svr = "";
if(!StringUtils.isBlank(blockMd5))
{
	md5Svr = Md5Tool.fileToMD5(ostm);

	if(!StringUtils.equals(md5Svr,blockMd5))
	{
		JSONObject o = new JSONObject();
		o.put("fileSvr",JSONObject.fromObject(fileSvr));
		o.put("md5Svr",md5Svr);
		o.put("md5Loc",blockMd5);
		wb.send_erro("blockMd5ValidFail",o);
		return;
	}
}

PathBuilder pb = new PathBuilder();
fileSvr.pathSvr = pb.relToAbs(fileSvr.pathSvr);

//块存储器
FileBlockWriter fw = ConfigReader.blockWriter();
boolean needGenId = !StringUtils.isEmpty(pid);//仅子文件创建 
if(needGenId) needGenId = 1 == fileSvr.blockIndex;
if(needGenId&&fw.storage == StorageType.FastDFS) needGenId = StringUtils.isEmpty(object_id);
if(needGenId&&fw.storage == StorageType.Minio) needGenId = StringUtils.isEmpty(object_id);
//子文件.仅第一块生成存储.id
try {
       if(needGenId) fileSvr.object_id  = fw.make(fileSvr);
       if(!StringUtils.isEmpty(fileSvr.pidRoot)) fileSvr.saveScheme();//保存到层级信息
       	
	//保存文件块数据
	fw.write(fileSvr,ostm);
	//合并文件
       if(isLastBlock) fw.writeLastPart(fileSvr);
       
   } catch (IOException e) {
       e.printStackTrace();
       response.setStatus(500);
       out.write(e.getMessage());
       return;
   }

up6_biz_event.file_post_block(id,Integer.parseInt(blockIndex));

JSONObject o = new JSONObject();
o.put("msg", "ok");
o.put("md5", md5Svr);
o.put("offset", blockOffset);
JSONObject fds = new JSONObject();
fds.put("object_id",fileSvr.object_id);//将UploadId回传给控件
if(1==fileSvr.blockIndex) o.put("fields",fds);
blockData.delete();
out.write(o.toString());
%>