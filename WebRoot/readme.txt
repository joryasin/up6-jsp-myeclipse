环境：
jdk:1.8
tomcat:8.0

----------------------------------------------------------------------------
更新记录：
2022-11-15
	优化块验证逻辑
	加密解密增加字节对齐
2022-06-10
    完善FastDFS错误处理
    完善minio错误处理逻辑
2022-06-01
    增加对minio的支持
2022-05-09
	增加对FastDFS的支持
2022-05-056
	目录结构分析改为使用结构信息文件
2020-12-29
	增加人大金仓数据库支持。
2019-05-23
	文件下载增加验证逻辑。
	  f_down.jsp
2019-05-23
	增加文件管理器。